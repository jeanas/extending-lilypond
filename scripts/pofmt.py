"""Rewrap PO files.

This ensures uncluttered diffs.  Different editors have different line
length conventions for PO files, and different Gettext versions as
well. This script reformats using no wrapping at all, which
corresponds to the output when using Emacs' PO mode and further
minimizes diffs.

Also removes obsolete entries, they accumulate and clutter the files.
(disabled for now, see comment)
"""

import argparse
from pathlib import Path

from babel.messages.pofile import read_po, write_po

parser = argparse.ArgumentParser()
parser.add_argument("dir")
args = parser.parse_args()
for path in Path(args.dir).rglob("*.po"):
    print(f"Cleaning up {str(path)}")
    with open(path, "rb") as file:
        po = read_po(file)
    po.obsolete = {}
    with open(path, "wb") as file:
        write_po(file, po, width=0)
