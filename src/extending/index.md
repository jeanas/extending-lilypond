# Extending guide

This guide attempts to cover all the important techniques for extending
LilyPond. As you will see, understanding them is very much concomitant with
understanding how LilyPond works internally.

LilyPond also has an official [Extending Manual](extending:index). Sadly, it is
nowhere near complete. The present guide serves as a substitute for the
unwritten parts of the Extending Manual.

The guide is structured as follows:

- First, a general introduction to extending, with examples of the fundamental
  tools, and an overview of how LilyPond works internally;

- Details on the interplay between LilyPond and Scheme code, and the different
  ways of inserting Scheme in LilyPond;

- A focus on music objects and how to create and transform them;

- A jump forward in LilyPond's compilation process, to the backend, explaining
  how grobs work and how to tweak them;

- Finally, a jump back to the translation step, which is the bridge between
  music and the backend; this part reuses notions from the two previous parts.

```{toctree}
intro
lily-and-scheme
music
backend
translation
properties-types
```
